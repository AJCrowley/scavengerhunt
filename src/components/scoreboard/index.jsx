import React, { Component } from 'react';
import Config from '../../config';
import style from './scoreboard.css';

class ScoreBoard extends Component {
	constructor() {
		super();
		this.state = {
			scores: [],
			hasErrored: false,
			isLoading: false,
		};
	}

	componentDidMount() {
		this.fetchData(`https://us-central1-nhf-scavenger-hunt.cloudfunctions.net/app/api/leaderboard/get/${Config.scoresToFetch}`);
	}

	fetchData(url) {
		this.setState({ isLoading: true });
		fetch(url)
			.then((response) => {
				this.setState({ isLoading: false });
				return response;
			})
			.then(response => response.json())
			.then((scores) => {
				this.setState({ scores: scores.scores });
			})
			.catch(() => this.setState({ hasErrored: true }));
	}

	getScores() {
		if (this.state.hasErrored) {
			return <p>Sorry! There was an error loading the items</p>;
		}
		if (this.state.isLoading) {
			return <p>Loading…</p>;
		}
		return (
			<ul>
				{this.state.scores.length > 0 && this.state.scores.map((score, index) => (
					<li key={index}>
						{score.name}: {score.charmsFound}, {score.score}
					</li>
				))}
			</ul>
		);
	}

	render() {
		return <div className={style['scoreboard']}>{this.getScores()}</div>;
	}
}
export default ScoreBoard;
