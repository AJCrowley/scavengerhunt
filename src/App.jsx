import React, { Component } from 'react';
import { hot } from 'react-hot-loader';
import ScoreBoard from './components/scoreboard';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	render() {
		return <ScoreBoard />;
	}
}

export default hot(module)(App);
